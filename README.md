# Arch Linux installation

## Bootable USB

- [Download](https://archlinux.org/download/) ISO and GPG files
- Verify the ISO file: `$ pacman-key -v archlinux-<version>-dual.iso.sig`
- Create a bootable USB with: `dd if=archlinux*.iso of=/dev/sdX && sync`

## UEFI setup

- Set boot mode to UEFI, disable Legacy mode entirely.
- Temporarily disable Secure Boot.
- Make sure a strong UEFI administrator password is set.
- Delete preloaded OEM keys for Secure Boot, allow custom ones.
- Set SATA operation to AHCI mode.

## Run installation

- Connect to wifi via: `iwctl station wlan0 connect WIFI-NETWORK`
- Run: `bash <(curl -sL https://gitlab.com/naacbin/dotfiles/-/raw/main/install.sh)`

## Post installation 

- Run : `sudo ./setup-system.sh`
- Edit `~/.config/chezmoi/chezmoi.toml` and execute `chezmoi apply`

### SysAdmin

```bash
sudo pacman -Syu vagrant ansible
vagrant plugin install vagrant-reload
pip install pywinrm
```
