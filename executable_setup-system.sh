#!/bin/bash

set -e
exec 2> >(while read line; do echo -e "\e[01;31m$line\e[0m"; done)

systemctl_enable_start() {
	echo "systemctl enable --now "$1""
	systemctl enable "$1"
	systemctl start "$1"
}

echo -e "\n### Configuring package..."
updatedb

echo -e "\n### VM config..."
sed -E "s/^#(unix_sock_group)/\1/g" -i /etc/libvirt/libvirtd.conf
sed -E "s/^#(unix_sock_rw_perms)/\1/g" -i /etc/libvirt/libvirtd.conf
systemctl restart libvirtd.service
virsh net-list --all
virsh net-autostart default
virsh net-start default
virsh net-list --all

echo -e "\n### Bluetooth config..."
modprobe btusb

echo -e "\n### External monitor backlight management..."
modprobe i2c-dev
cp /usr/share/ddcutil/data/45-ddcutil-i2c.rules /etc/udev/rules.d/

echo -e "\n### Battery management..."
sed -E "s/^#(TLP_ENABLE).*/\1=1/g" -i /etc/tlp.conf
sed -E "s/^#(TLP_DEFAULT_MODE).*/\1=BAT/g" -i /etc/tlp.conf
sed -E "s/^#(USB_AUTOSUSPEND).*/\1=0/g" -i /etc/tlp.conf
sed -E "s/^#(START_CHARGE_THRESH_BAT0).*/\1=60/g" -i /etc/tlp.conf
sed -E "s/^#(STOP_CHARGE_THRESH_BAT0).*/\1=85/g" -i /etc/tlp.conf

echo -e "\n### Usbguard rules..."
usbguard generate-policy >/etc/usbguard/rules.conf
chmod 600 /etc/usbguard/rules.conf

echo -e "\n### Automount disk on plug..."
cat <<EOF >/etc/udev/rules.d/99-udisks2.rules
# UDISKS_FILESYSTEM_SHARED
# ==1: mount filesystem to a shared directory (/media/VolumeName)
# ==0: mount filesystem to a private directory (/run/media/\$user/VolumeName)
# See udisks(8)
ENV{ID_FS_USAGE}=="filesystem|other|crypto", ENV{UDISKS_FILESYSTEM_SHARED}="1"
EOF
mkdir /media
echo "D /media 0755 root root 0 -" >/etc/tmpfiles.d/media.conf

echo -e "\n### Fix Keychron K6 F1-F12 linux..."
echo "options hid_apple fnmode=2" >/etc/modprobe.d/hid_apple.conf

echo -e "\n### Enabling and starting services..."

sysctl --system >/dev/null

systemctl_enable_start "docker.socket"
systemctl_enable_start "libvirtd.service"
systemctl_enable_start "tlp.service"
systemctl_enable_start "usbguard.service"
# usbguard -> for program interacting via API (such as the CLI)
# systemctl_enable_start "usbguard-dbus.service"
# usbguard-dbus -> for program interacting via DBUS (such as the Qt GUI)
systemctl_enable_start "bluetooth.service"
systemctl_enable_start "iwd.service"
systemctl_enable_start "dhcpcd.service"
systemctl_enable_start "nftables.service"

