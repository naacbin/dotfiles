#!/bin/bash

set -e
exec 2> >(while read line; do echo -e "\e[01;31m$line\e[0m"; done)

systemctl_enable_start() {
	echo "systemctl enable --now "$1""
	sudo systemctl enable --now "$1"
}

is_chroot() {
	! cmp -s /proc/1/mountinfo /proc/self/mountinfo
}

echo -e "\n### Installing oh-my-zsh..."
git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh

echo -e "\n### Cloning dotfiles..."
mkdir -p ~/.config/chezmoi/ 2>/dev/null || true
cat <<EOF >~/.config/chezmoi/chezmoi.toml
[data]
  output_left = ""
  output_middle = ""
  output_right = ""
  sink_laptop = ""
  source_laptop = ""
  sink_monitor = ""
  sink_headset = ""
  source_headset = ""
  bt_mac_speaker = ""
  bt_id_speaker = ""
  bt_mac_headset = ""
  bt_id_headset = ""
  name = "Naacbin"
  email = ""
  git_dir1 = ""
EOF
chezmoi init --apply https://gitlab.com/naacbin/dotfiles.git

echo -e "\n###Installing applications..."

git clone https://aur.archlinux.org/yay.git
pushd yay
makepkg -si
popd
rm -rf yay

yay -Sy spotify joplin-appimage vscodium-bin vscodium-bin-features vscodium-bin-marketplace moonlight-qt
# virtualbox-ext-oracle
# vscodium-bin-features -> enable remote ssh
# vscodium-bin-marketplace -> enable vscode marketplace

yay -Sy mullvad-vpn

if is_chroot; then
	echo "=== Running in chroot, skipping..." >&2
else
	echo -e "\n### Setting dark theme on GTK application..."

	gsettings set org.gnome.desktop.interface gtk-theme "Arc-Dark"
	gsettings set org.gnome.desktop.interface color-scheme "prefer-dark"

	echo -e "\n### Enabling and starting services..."
	systemctl_enable_start "mullvad-daemon.service"
fi
