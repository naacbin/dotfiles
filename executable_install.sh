#!/bin/bash

set -uo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR

exec 1> >(tee "stdout.log")
exec 2> >(tee "stderr.log" >&2)

MOUNT_POINT="/mnt/iso"

hostname=""
timezone=""
user=""
password=""
root_part_password=""
device=""
kernel=""
font=""
hibernation="false"

# Dialog
BACKTITLE="Arch Linux installation"

get_input() {
	title="$1"
	description="$2"

	input=$(dialog --clear --stdout --backtitle "$BACKTITLE" --title "$title" --inputbox "$description" 0 0)
	echo "$input"
}

get_password() {
	title="$1"
	description="$2"

	init_pass=$(dialog --clear --stdout --backtitle "$BACKTITLE" --title "$title" --passwordbox "$description" 0 0)
	: ${init_pass:?"password cannot be empty"}

	test_pass=$(dialog --clear --stdout --backtitle "$BACKTITLE" --title "$title" --passwordbox "$description again" 0 0)
	if [[ "$init_pass" != "$test_pass" ]]; then
		echo "Passwords did not match" >&2
		exit 1
	fi
	echo $init_pass
}

get_choice() {
	title="$1"
	description="$2"
	shift 2
	options=("$@")
	dialog --clear --stdout --backtitle "$BACKTITLE" --title "$title" --menu "$description" 0 0 0 "${options[@]}"
}

echo -e "\n### Checking UEFI boot mode..."
if [ ! -f /sys/firmware/efi/fw_platform_size ]; then
	echo "You must boot in UEFI mode to continue." >&2
	exit 2
else
	echo -e "UEFI mode detected. Continuing..."
fi

echo -e "\n### Installing additional tools..."
pacman-key --init
pacman-key --populate
pacman -Sy --noconfirm --needed terminus-font dialog

if [ -z "$font" ]; then
	echo -e "\n### HiDPI screens"
	noyes=("Yes" "The font is too small" "No" "The font size is just fine")
	hidpi=$(get_choice "Font size" "Is your screen HiDPI?" "${noyes[@]}") || exit 1
	clear
	# If the screen is HiDPI, choose the larger font, else choose the regular one
	[[ "$hidpi" == "Yes" ]] && font="ter-118n" || font="ter-716n"
	setfont "${font}"
fi

hostname=${hostname:-$(get_input "Hostname" "Enter hostname")}
clear
: ${hostname:?"hostname cannot be empty"}

echo -e "\n### Setting up clock..."
timedatectl set-ntp true
hwclock --systohc

echo -e "\n### Setting up timezone..."
tz_options=(
	"America/New_York" "Eastern Time (US)"
	"America/Los_Angeles" "Pacific Time (US)"
	"Europe/London" "GMT / BST"
	"Europe/Paris" "CET / CEST"
)
timezone=${timezone:-$(get_choice "Timezone" "Select your timezone:" "${tz_options[@]}")}
clear
: ${timezone:?"Timezone must be selected. Exiting."}
timedatectl set-timezone ${timezone}

# Validate that a timezone was selected.
: ${timezone:?"Timezone must be selected. Exiting."}

user=${user:-$(get_input "User" "Enter username")}
clear
: ${user:?"user cannot be empty"}

password=${password:-$(get_password "User" "Enter password")}
clear
: ${password:?"password cannot be empty"}

root_part_password=${root_part_password:-$(get_password "Root partition" "Enter password")}
clear
: ${root_part_password:?"password cannot be empty"}

devicelist=$(lsblk -dplnx size -o name,size | grep -Ev "boot|rpmb|loop" | tac | tr '\n' ' ')
read -r -a devicelist <<< $devicelist

device=${device:-$(get_choice "Installation" "Select installation disk" "${devicelist[@]}")}
clear
: ${device:?"No installation disk selected"}

# Ask for confirmation.
device_confirmation=$(get_choice "Confirm Disk" \
	"=====WARNING=====\nThe disk: ${device}\nwill be completely wiped.\nAre you sure you want to continue?" \
	"Yes" "Wipe the disk" \
	"No" "Cancel installation")
clear

if [[ "${device_confirmation}" != "Yes" ]]; then
	echo "Installation aborted by the user."
	exit 1
fi

echo -e "\n### Kernel Selection..."
kernel_options=("linux" "Latest (Classic)" "linux-lts" "Long-Term Support (LTS)")
kernel=${kernel:-$(get_choice "Kernel Selection" "Choose the Linux kernel version:" "${kernel_options[@]}")}
clear
: ${kernel:?"No kernel selected"}

echo -e "\n### Setting up partitions..."
umount -R ${MOUNT_POINT} 2> /dev/null || true
swapoff /dev/mapper/vg-swap 2> /dev/null || true
vgchange -a n vg 2> /dev/null || true
cryptsetup luksClose cryptlvm 2> /dev/null || true

lsblk -plnx size -o name "${device}" | xargs -n1 wipefs --all
sgdisk --clear "${device}" --new 1::256M --typecode 1:ef00 "${device}" --new 2:: "${device}"
sgdisk --change-name=1:ESP --change-name=2:primary "${device}"

part_boot="$(ls ${device}* | grep -E "^${device}p?1$")"
part_root="$(ls ${device}* | grep -E "^${device}p?2$")"

echo -e "\n### Formatting partitions..."
echo -n ${root_part_password} | cryptsetup luksFormat -s 512 -h sha512 -i 5000 "${part_root}"
echo -n ${root_part_password} | cryptsetup open "${part_root}" cryptlvm

ram_size=$(awk '/MemTotal/ {printf "%.0f", $2/1024/1024}' /proc/meminfo)
if [ "$hibernation" == "true" ]; then
	# If hibernation is needed, swap should be the same size as RAM
	swap_size=$ram_size
	echo "Hibernation enabled. Swap size set to: ${swap_size}G"
else
	# If hibernation is not needed, swap is half of the RAM size
	swap_size=$((ram_size / 2))
	echo "Hibernation not enabled. Swap size set to: ${swap_size}G"
fi

echo -e "\n### Creating lvm..."
pvcreate /dev/mapper/cryptlvm
vgcreate vg /dev/mapper/cryptlvm
lvcreate -L ${swap_size}G vg -n swap
lvcreate -L 4G vg -n tmp
lvcreate -L 200G vg -n root
lvcreate -l 100%FREE vg -n home

echo -e "\n### Setting up partitions..."
mkfs.ext4 /dev/vg/root
mkfs.ext4 /dev/vg/home
mkfs.ext4 /dev/vg/tmp
mkswap /dev/vg/swap
mkfs.fat -F32 "${part_boot}"

echo -e "\n### Mounting partitions..."
mkdir -p ${MOUNT_POINT}
mount /dev/vg/root ${MOUNT_POINT}
mkdir -p ${MOUNT_POINT}/{home,tmp,boot}
mount "${part_boot}" ${MOUNT_POINT}/boot
mount /dev/vg/home ${MOUNT_POINT}/home
mount /dev/vg/tmp ${MOUNT_POINT}/tmp
swapon /dev/vg/swap

echo -e "\n### Installing base packages..."
# Get CPU vendor information
cpu_vendor=$(lscpu | grep -i "Vendor ID" | awk '{ print $NF }')
echo "Detected CPU vendor: $cpu_vendor"

# Install appropriate microcode package based on CPU vendor
if [[ "$cpu_vendor" == "GenuineIntel" ]]; then
	microcode="intel-ucode"
elif [[ "$cpu_vendor" == "AuthenticAMD" ]]; then
	microcode="amd-ucode"
else
	echo "Unsupported CPU vendor: $cpu_vendor" >&2
	exit 1
fi

packages=(
	base base-devel ${microcode} ${kernel} ${kernel}-headers linux-firmware lvm2
	iptables-nft nftables iwd openresolv dhcpcd
	terminus-font man-db man-pages vim git reflector
	zsh
)
pacstrap ${MOUNT_POINT} "${packages[@]}"

echo -e "\n### Generating fstab..."
genfstab -U ${MOUNT_POINT} >> ${MOUNT_POINT}/etc/fstab
# Improve performance and lifetime of disk by disabling update of the file access time
sed -i 's/relatime/noatime/g' ${MOUNT_POINT}/etc/fstab

echo -e "\n### Set keymap and font..."
echo "KEYMAP=us" > ${MOUNT_POINT}/etc/vconsole.conf
echo "FONT=${font}" >> ${MOUNT_POINT}/etc/vconsole.conf

echo -e "\n### Set hostname..."
echo "${hostname}" > ${MOUNT_POINT}/etc/hostname
echo -e "127.0.0.1 localhost\n127.0.1.1 ${hostname}.localdomain ${hostname}" > ${MOUNT_POINT}/etc/hosts

echo -e "\n### Manage locale..."
sed -i '/en_US.UTF-8/s/^#//g' ${MOUNT_POINT}/etc/locale.gen
echo "LANG=en_US.UTF-8" > ${MOUNT_POINT}/etc/locale.conf
ln -sf /usr/share/zoneinfo/${timezone} ${MOUNT_POINT}/etc/localtime
arch-chroot ${MOUNT_POINT} locale-gen

echo -e "\n### Setup mkinitcpio..."
sed -i 's/^HOOKS.*/HOOKS=(base udev keyboard autodetect microcode modconf keymap consolefont block encrypt lvm2 filesystems fsck)/g' ${MOUNT_POINT}/etc/mkinitcpio.conf
arch-chroot ${MOUNT_POINT} mkinitcpio -P

echo -e "\n### Setting boot config..."
arch-chroot ${MOUNT_POINT} bootctl --path=/boot/ install
echo -e "default arch\ntimeout 3\neditor 0" > ${MOUNT_POINT}/boot/loader/loader.conf
echo -e "title Arch Linux\nlinux /vmlinuz-${kernel}\ninitrd /initramfs-${kernel}.img" > ${MOUNT_POINT}/boot/loader/entries/arch.conf
uuid=$(blkid | grep ${part_root} | cut -d " " -f 2 | sed 's/"//g')
echo "options cryptdevice=${uuid}:cryptlvm root=/dev/vg/root rw" >> ${MOUNT_POINT}/boot/loader/entries/arch.conf

echo -e "\n### Allow wheel group to run sudo command"
sed -E 's/# (%wheel ALL=\(ALL:ALL\) ALL)/\1/g' -i ${MOUNT_POINT}/etc/sudoers

echo -e "\n### Creating user..."
arch-chroot ${MOUNT_POINT} useradd -m -s /usr/bin/zsh "${user}"
# Add needed group : network i2c
# brightnessctl need 'video' to manage backlight and 'input' for led
# 'docker' to manage docker
# 'libvirt' to launch VM
for group in wheel network video input i2c docker libvirt; do
	arch-chroot ${MOUNT_POINT} groupadd -rf "${group}"
	arch-chroot ${MOUNT_POINT} gpasswd -a "${user}" "${group}"
done
arch-chroot ${MOUNT_POINT} chsh -s /usr/bin/zsh
echo "${user}:${password}" | arch-chroot ${MOUNT_POINT} chpasswd
# Delete password and lock the password
arch-chroot ${MOUNT_POINT} passwd -dl root

echo -e "\n### Setting up mirrorlist..."
reflector --verbose -c fr,de -l 20 -p https --sort rate --save ${MOUNT_POINT}/etc/pacman.d/mirrorlist

echo -e "\n### Configuring pacman..."
sed -E 's/^#(Color)/\1/g' -i ${MOUNT_POINT}/etc/pacman.conf
sed -E 's/^#(ParallelDownloads).*/\1 = 20/g' -i ${MOUNT_POINT}/etc/pacman.conf

echo -e "\n### Enable network by default..."
mkdir ${MOUNT_POINT}/etc/iwd/ 2> /dev/null || true
cat <<EOF > ${MOUNT_POINT}/etc/iwd/main.conf
[General]
EnableNetworkConfiguration=true
[Network]
NameResolvingService=resolvconf
EOF

echo -e "\n### Running initial setup"
arch-chroot ${MOUNT_POINT} bash <(curl -sL https://gitlab.com/naacbin/dotfiles/-/raw/main/install-package.sh)
arch-chroot ${MOUNT_POINT} sudo -u ${user} bash -c 'bash <(curl -sL https://gitlab.com/naacbin/dotfiles/-/raw/main/setup-user.sh)'

echo -e "\n### DONE - reboot and re-run both ./setup-*.sh scripts"

echo -e "\n### Reboot now, and after power off remember to unplug the installation USB"
mount -R ${MOUNT_POINT}
