#!/bin/bash

set -e
exec 2> >(while read line; do echo -e "\e[01;31m$line\e[0m"; done)

echo -e "\n###Adding PGP key..."

pacman-key --init
pacman-key --populate archlinux

echo -e "\n###Installing package..."

pacman -Sy --noconfirm mlocate openssh chezmoi zsh-autosuggestions zsh-syntax-highlighting zsh-completions bash-completion

### VPN
pacman -Sy --noconfirm wireguard-tools openvpn

### Fonts
pacman -Sy --noconfirm noto-fonts noto-fonts-emoji ttf-font-awesome nwg-look arc-gtk-theme papirus-icon-theme ttf-droid
# noto-fonts-emoji -> discord
# ttf-font-awesome -> font awesome on waybar
# nwg-look -> set theme of other app (native wayland)
# arc-gtk-theme -> lxappearance theme
# ttf-droid -> extend to chinese

### Wayland stuff
pacman -Sy --noconfirm wayland sway swaylock swayidle swaybg waybar kanshi alacritty mako rofi-wayland rofi-calc xorg-xwayland libappindicator-gtk3 xwaylandvideobridge
# rofi-wayland -> menu launcher
# kanshi -> handle hotplug monitor config
# libappindicator-gtk3 -> tray icon on waybar
# xorg-xwayland -> require in order to run most graphical apps
# xwaylandvideobridge -> screencast Wayland windows with X11 applications

### Tools
pacman -Sy --noconfirm brightnessctl usbguard acpi playerctl grim slurp wf-recorder wl-clipboard swappy xdg-utils # xdg-desktop-portal-wlr xorg-xhost
# brightnessctl (need reboot to set video/input group managing brightness by udev rules)
# usbguard -> protect usb port
# acpi -> battery status
# playerctl -> spotify
# grim -> screen capture
# slurp -> select region of the screen
# wf-recorder -> record screen
# wl-clipboard -> wayland clipboard
# swappy -> photo editor
# xdg-utils -> open link from app into browser
# xdg-desktop-portal-wlr -> use in order to get flameshot work on wayland and screen sharing on firefox

### Audio/bluetooth stuff
pacman -Sy --noconfirm pipewire wireplumber pipewire-alsa pipewire-pulse bluez bluez-utils
# Force install of conflicted package https://unix.stackexchange.com/questions/274727/how-to-force-pacman-to-answer-yes-to-all-questions/584001#584001
pacman -Sy --noconfirm pipewire-jack --ask 4
# bluez-utils -> bluetoothctl

### Sysadmin tools
pacman -Sy --noconfirm neovim eza fd bat ripgrep dust bottom hexyl ranger tmux zip unzip wget lsof tree imagemagick usbutils dmidecode sysstat traceroute jq fzf rsync imv
# eza -> replace ls
# fd -> replace find
# bat -> replace cat
# ripgrep -> replace grep
# dust -> replace du
# bottom -> replace htop
# hexyl -> command line hex viewer
# ranger -> cli file explorer
# dmidecode -> check BIOS configuration
# jq -> json parsing
# imv -> image viewer

### Other app
pacman -Sy --noconfirm python-pip

### Graphical application
pacman -Sy --noconfirm firefox chromium nextcloud-client element-desktop signal-desktop discord vlc qbittorrent libreoffice-fresh thunar pavucontrol
# thunar -> file explorer
# pavucontrol -> volume mixer

### Archive GUI
pacman -Sy --noconfirm xarchiver thunar-archive-plugin

### Security tools
#yay -Sy exiftool gdb ltrace strace radare2 iaito

### Docker
pacman -Sy --noconfirm docker docker-compose

### VM
pacman -Sy --noconfirm qemu virt-manager virt-viewer virtualbox virtualbox-host-modules-arch virtualbox-guest-iso dnsmasq
# dnsmasq -> required for default NAT/DHCP for guests in libvirt
# virtualbox-host-modules-lts

### Monitor settings management
pacman -Sy --noconfirm ddcutil

### Battery management
pacman -Sy --noconfirm tlp
# acpi_call

### Disk management
pacman -Sy --noconfirm udisks2 udiskie ntfs-3g gvfs gvfs-smb
# gvfs -> mounting and trash functionality
# gvfs-smb -> Windows File and printer support
# udisks2 -> mount (bakcend of gvfs)
# udiskie -> automount
# ntfs-3g -> support ntfs devices
