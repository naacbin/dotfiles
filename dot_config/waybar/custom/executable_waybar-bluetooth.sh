#!/bin/sh
# listen() {
#     dbus-monitor --system --profile "interface='org.freedesktop.DBus.Properties'" |
#         while read -r line; do
#             blocked="$(usbguard list-devices -b | head -n1 | grep -Po 'name "\K[^"]+')"
#             if [ -n "$blocked" ]; then
#                 printf "{\"text\": \"<span foreground='#928374'></span>\"}\n" "$blocked"
#             else
#                 printf '{"text": ""}\n'
#             fialias l='ls -alh'
#         done
# }

if [ $(bluetoothctl show | grep "Powered: yes" | wc -c) -eq 0 ]
then
  printf "{\"text\": \"\"}\n"
else
  if [ $(echo info | bluetoothctl | grep 'Device' | wc -c) -eq 0 ]
  then
    printf "{\"text\": \"\", \"class\":\"disconnected\"}\n"
  else
    printf "{\"text\": \"\", \"class\":\"connected\"}\n"
  fi
fi